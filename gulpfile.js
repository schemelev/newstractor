var gulp = require('gulp');
var rename = require("gulp-rename");
var iconfont = require('gulp-iconfont');
var consolidate = require('gulp-consolidate');

var config = require('./iconFont/iconFont_config.js');

gulp.task('iconFont', function(){
  return gulp.src([config.level + '/' + config.block + '/icons/*.svg'])
    .pipe(iconfont({
      fontName: config.block,
      appendUnicode: true,
      formats: ['ttf', 'eot', 'woff', 'svg'/*, 'woff2'*/],
      normalize: true
    }))
    .on('glyphs', function(glyphs, options) {
      console.log(glyphs, options);
      gulp.src('iconFont/templates/bem__style')
        .pipe(consolidate('lodash', {
          glyphs: glyphs.map(function(glyph) {
            return { name: glyph.name, codepoint: glyph.unicode[0].charCodeAt(0) }
          }),
          blockMod: config.blockMod,
          fontName: config.block,
          fontPath: './',
          className: config.block
        }))
        .pipe(rename({
          basename: config.block,
          extname: '.styl'
        }))
        .pipe(gulp.dest(config.level + '/' + config.block));
    })
    .pipe(gulp.dest(config.level + '/' + config.block));
});