modules.define('header', ['i-bem__dom', 'jquery'], function (provide, BEMDOM, $) {
  provide(BEMDOM.decl(this.name, {

    onSetMod: {
      js: {
        inited: function() {

          this._search = this.findBlockInside('search');
          this._sitename = this.findBlockInside('sitename');

          console.log(this._sitename);

          this._search
            .on('show-input', this._pullSitename, this);

        }
      }
    },

    _pullSitename: function() {
      if(arguments[1]){
        if(arguments[1].show){
          this._sitename.setMod('pull', 'left');
        } else {
          this._sitename.delMod('pull');
        }
      }
    }

  }));
});