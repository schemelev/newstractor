modules.define('offcanvas', ['i-bem__dom', 'jquery'], function (provide, BEMDOM, $) {
  provide(BEMDOM.decl(this.name, {

    onSetMod: {
      js: {
        inited: function() {

          var _this = this;

          $( window ).resize(function() {
            _this._setHeights();
          });

        }
      },
      show: {
        '*': function() {
          console.log(this.getMod('show-left'));
        }
      }
    },

    _setHeights: function() {
      var height = Math.max(this.elem('left').outerHeight(), this.elem('right').outerHeight(), this.elem('content').outerHeight(), $(window).outerHeight());

      this.elem('left')
        .css({
          height: height
        });

      this.elem('overlay')
        .css({
          height: height
        });

      this.elem('right')
        .css({
          height: height
        });
    },

    _onShowLeftClick: function(e) {
      this.setMod('show-left', 'true');
      this._setHeights();
    },
    _onCloseLeftClick: function(e) {
      this.delMod('show-left');
    }

  }, {
    live : function() {
      this.liveBindTo('show-left', 'click', function(e) {
        e.preventDefault();
        this._onShowLeftClick(e);
      });
      this.liveBindTo('close-left', 'click', function(e) {
        e.preventDefault();
        this._onCloseLeftClick(e);
      });
      this.liveBindTo('overlay', 'click', function(e) {
        e.preventDefault();
        this._onCloseLeftClick(e);
      });
      return false;
    }
  }));
});