modules.define('search', ['i-bem__dom', 'jquery'], function (provide, BEMDOM, $) {
    provide(BEMDOM.decl(this.name, {

        onSetMod: {
            js: {
                inited: function() {

                    this._button = this.findBlockInside('button');
                    this._input = this.findBlockInside('input');

                    this._button
                        .on('click', this._onClickButton, this);

                }
            }
        },

        _onClickButton: function() {
            if(!this.hasMod('show-input')) {
                this._input.domElem.find('input').focus();
            }
            this.toggleMod('show-input', 'true', '');
            this.emit('show-input', { show: this.hasMod('show-input') });
        }

    }, {
        /*live : function() {
            this.liveBindTo('show-left', 'click', function(e) {
                e.preventDefault();
                this._onShowLeftClick(e);
            });
            return false;
        }*/
    }));
});