module.exports = {
  block: 'page',
  title: 'NewsTractor',
  head: [
    {
      elem: 'meta',
      attrs : { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    }
  ],
  styles: [
    { elem: 'css', url: 'index.min.css' },
    { elem: 'css', url: 'https://fonts.googleapis.com/css?family=Open+Sans:800,700,400,300&subset=latin,cyrillic' },
    { elem: 'css', url: 'https://fonts.googleapis.com/css?family=Montserrat:700' }
  ],
  scripts: [
    { elem: 'js', url: 'index.min.js' }
  ],
  content: {
    block: 'offcanvas',
    js: true,
    content: [
      {
        elem: 'left',
        content: [
          {
            elem: 'close-left',
            content: {
              block: 'nt-icon',
              mods: { name: 'close' }
            }
          },
          {
            elem: 'left-menu',
            content: {
              block : 'menu',
              mods: { align: 'center', focused: false, type: 'main' },
              mix: { block: 'offcanvas', elem: 'left-menu' },
              content : [
                {
                  title: 'Лента',
                  val: 'lenta',
                  url: '#'
                },
                {
                  title: 'Списки',
                  val: 'lists',
                  url: '#'
                },
                {
                  title: 'Источники',
                  val: 'sources',
                  url: '#'
                },
                {
                  title: 'Избранное',
                  val: 'favorites',
                  url: '#'
                },
                {
                  title: 'Картиночки',
                  val: 'images',
                  url: '#'
                },
                {
                  title: 'Видосы',
                  val: 'videos',
                  url: '#'
                }
              ].map(function(item) {
                  return [
                    {
                      block : 'menu-item',
                      mods: {
                        checked: item.val === 'lenta'
                      },
                      val: item.val,
                      content : {
                        block: 'link',
                        url: item.url,
                        content: item.title
                      }
                    }
                  ]
                })
            }
          },
          {
            elem: 'copyright',
            content: {
              block: 'copyright',
              content: {
                tag: 'p',
                content: 'Копирование без<br>регистрации запрещено'
              }
            }
          }
        ]
      },
      {
        elem: 'content',
        content: [
          {
            block: 'header',
            content: {
              block: 'container',
              content: [
                {
                  block : 'row',
                  content : [
                    {
                      elem : 'col',
                      mods : { md : 4, ms : 4, xs: 3 },
                      content : {
                        block: 'offcanvas',
                        elem: 'show-left',
                        mix: { block: 'header', elem: 'open-menu' },
                        content: {
                          block: 'nt-icon',
                          mods: { name: 'open-menu' }
                        }
                      }
                    },
                    {
                      elem : 'col',
                      mods : { md : 4, ms : 4, xs: 6 },
                      content : {
                        block: 'header',
                        elem: 'sitename',
                        content: {
                          block: 'sitename',
                          content: 'NewsTractor'
                        }
                      }
                    },
                    {
                      elem : 'col',
                      mods : { md : 4, ms : 4, xs: 3 },
                      content : [
                        /*{
                          block: 'header',
                          elem: 'settings',
                          content: []
                        },*/
                        {
                          block: 'header',
                          elem: 'search',
                          content: [
                            {
                              block: 'button',
                              mix: { block: 'header', elem: 'settings' },
                              content: {
                                block: 'nt-icon',
                                mods: { name: 'settings' }
                              }
                            },
                            {
                              block: 'search',
                              mods: { header: true },
                              js: true,
                              content: [
                                {
                                  block: 'input',
                                  mix: [
                                    { block: 'search', elem: 'input' },
                                    { block: 'header', elem: 'search-input' }
                                  ],
                                  name: 'query',
                                  placeholder: 'поиск'
                                },
                                {
                                  block: 'button',
                                  mix: { block: 'search', elem: 'button' },
                                  content: {
                                    block: 'nt-icon',
                                    mods: { name: 'search' }
                                  }
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          },
          {
            block: 'columns',
            js: true,
            content: {
              block: 'container',
              content: {
                block: 'row',
                content: [
                  {
                    elem: 'col',
                    mix: { block: 'columns', elem: 'aside-left' },
                    mods: { md: 2, ms: 3 },
                    content: {
                      block: 'aside',
                      mods: { side: 'left' },
                      content: [
                        {
                          block: 'title',
                          mix: { block: 'aside', elem: 'title' },
                          mods: { header: 2, style: 'black', align: 'center' },
                          content: 'Источники'
                        },
                        {
                          elem: 'sources',
                          content: [
                            {
                              block: 'checkbox-group',
                              mix: { block: 'sources' },
                              mods: { type: 'line' },
                              name: 'source',
                              val: ['twitter'],
                              options: (function() {
                                return 'привет'.split('').map(function() {
                                  return [
                                    {
                                      title: 'Вконтакте',
                                      type: 'vk'
                                    },
                                    {
                                      title: 'Лента',
                                      type: 'lenta'
                                    },
                                    {
                                      title: 'Twitter',
                                      type: 'twitter'
                                    },
                                    {
                                      title: 'Facebook',
                                      type: 'facebook'
                                    },
                                    {
                                      title: 'Github',
                                      type: 'github'
                                    }
                                  ][Math.floor(Math.random() * 5)];
                                }).map(function(item) {
                                  return {
                                    val: item.type,
                                    text: {
                                      block: 'source',
                                      mods: { type: item.type },
                                      content: [
                                        {
                                          elem: 'icon',
                                          mods: { type: item.type },
                                          content: {
                                            block: 'nt-icon',
                                            mods: { name: item.type }
                                          }
                                        },
                                        {
                                          elem: 'title',
                                          content: item.title
                                        }
                                      ]
                                    }
                                  }
                                })
                              })()
                            },
                            {
                              elem: 'add-source',
                              content: {
                                block: 'link',
                                mix: { block: 'source', elem: 'add' },
                                url: '#',
                                content: {
                                  block: 'nt-icon',
                                  mods: { name: 'add' }
                                }
                              }
                            }
                          ]
                        }
                      ]
                    }
                  },
                  {
                    elem: 'col',
                    mods: { md: 7, ms: 9 },
                    content: [
                      {
                        block: 'columns',
                        elem: 'header',
                        content: [
                          {
                            block: 'title',
                            mix: { block: 'aside', elem: 'title' },
                            mods: { header: 1, display: 'inline' },
                            content: 'Последние новости'
                          },
                          {
                            block : 'menu',
                            mix: { block: 'columns', elem: 'header-menu' },
                            mods : { type: 'horizontal' },
                            val : [1],
                            content : [
                              {
                                block : 'menu-item',
                                val : 1,
                                content : {
                                  block: 'link',
                                  url: '#',
                                  content: 'всё'
                                }
                              },
                              {
                                block : 'menu-item',
                                val : 2,
                                content : {
                                  block: 'link',
                                  url: '#',
                                  content: 'лучшие'
                                }
                              }
                            ]
                          },
                          {
                            block: 'columns',
                            elem: 'header-controls',
                            content: {
                              block : 'menu',
                              mods : { type: 'horizontal' },
                              val : [1],
                              content : [
                                'lcr',
                                'c',
                                'lc',
                                'cr'
                              ].map(function(item) {
                                  return {
                                    block : 'menu-item',
                                    val : item,
                                    content : {
                                      block: 'columns',
                                      elem: 'control',
                                      mods: { type: item, active: item === 'lcr' },
                                      url: '#',
                                      content: {
                                        block: 'nt-icon',
                                        mods: { name: 'control-' + item }
                                      }
                                    }
                                  }
                                })
                            }
                          }
                        ]
                      },
                      {
                        block: 'columns',
                        elem: 'news-items',
                        content: [
                          {
                            block: 'columns',
                            elem: 'fresh-items',
                            content: '8 новых'
                          },
                          {
                            block: 'news',
                            content: {
                              block: 'row',
                              content: [
                                {
                                  sourceType: 'lenta',
                                  sourceTitle: 'lenta.ru',
                                  date: 'Сегодня 25.07.2015',
                                  timeForReading: '5 минут на прочтение',
                                  title: 'Ученые ищут святой Грааль',
                                  introtext: '<p>Кинокартина Кристофера Нолана «Интерстеллар» стала одним из главных кинохитов прошлого года во многом благодаря потрясающей визуализации различных феноменов астрофизики — таких, как черные дыры, гиперпространство и относительность времени. В своей книге физик Кип Торн раскрывает научную основу показанных чудес, а также подробно</p>',
                                  previewUrl: '',
                                  unknown: '12.1',
                                  comments: '17',
                                  questions: '9',
                                  imgCount: '+5',
                                  onTopic: '+5'
                                },
                                {
                                  sourceType: 'vk',
                                  sourceTitle: 'vk.com',
                                  date: 'Сегодня 25.07.2015',
                                  timeForReading: '5 минут на прочтение',
                                  title: 'Ученые ищут святой Грааль',
                                  introtext: '<p>Кинокартина Кристофера Нолана «Интерстеллар» стала одним из главных кинохитов прошлого года во многом благодаря потрясающей визуализации различных феноменов астрофизики — таких, как черные дыры, гиперпространство и относительность времени. В своей книге физик Кип Торн раскрывает научную основу показанных чудес, а также подробно</p>',
                                  previewUrl: '',
                                  unknown: '12.1',
                                  comments: '17',
                                  questions: '9',
                                  imgCount: '+5',
                                  onTopic: '+5'
                                },
                                {
                                  sourceType: 'lenta',
                                  sourceTitle: 'lenta.ru',
                                  date: '25.07.2015',
                                  timeForReading: '5 мин',
                                  title: 'Ученые ищут святой Грааль',
                                  introtext: '<p>Кинокартина Кристофера Нолана «Интерстеллар» стала одним из главных кинохитов прошлого года во многом благодаря потрясающей визуализации различных феноменов астрофизики — таких, как черные</p>',
                                  previewUrl: '',
                                  unknown: '12.1',
                                  comments: '17',
                                  questions: '9',
                                  imgCount: '+5',
                                  onTopic: '+5'
                                },
                                {
                                  sourceType: 'github',
                                  sourceTitle: 'github.com',
                                  date: '25.07.2015',
                                  timeForReading: '5 мин',
                                  title: 'Ученые ищут святой Грааль',
                                  introtext: '<p>Кинокартина Кристофера Нолана «Интерстеллар» стала одним из главных кинохитов прошлого года во многом благодаря потрясающей визуализации различных феноменов астрофизики — таких, как черные дыры, гиперпространство и относительность времени. В своей книге физик Кип Торн раскрывает</p>',
                                  previewUrl: '',
                                  unknown: '12.1',
                                  comments: '17',
                                  questions: '9',
                                  imgCount: '+5',
                                  onTopic: '+5'
                                }
                              ].map(function(item, index) {
                                  return index <= 1
                                    ? {
                                    elem: 'col',
                                    mods: { md: 12, type: 'wide-news-item' },
                                    content: {
                                      block: 'news-item',
                                      mods: { type: 'wide' },
                                      content: [
                                        {
                                          elem: 'header',
                                          content: [
                                            {
                                              elem: 'source-icon',
                                              content: {
                                                block: 'source',
                                                mix: {
                                                  elem: 'icon',
                                                  mods: { type: item.sourceType, 'news-item': true }
                                                },
                                                content: {
                                                  block: 'nt-icon',
                                                  mods: { name: item.sourceType }
                                                }
                                              }
                                            },
                                            {
                                              elem: 'first-line',
                                              content: [
                                                {
                                                  elem: 'source-title',
                                                  content: item.sourceTitle
                                                },
                                                {
                                                  block: 'time2read',
                                                  mix: { block: 'news-item', elem: 'time2read' },
                                                  content: [
                                                    {
                                                      block: 'nt-icon',
                                                      mix: { block: 'time2read', elem: 'icon' },
                                                      mods: { name: 'eye' }
                                                    },
                                                    {
                                                      elem: 'title',
                                                      content: item.timeForReading
                                                    }
                                                  ]
                                                },
                                                {
                                                  block: 'date',
                                                  mix: { block: 'news-item', elem: 'date' },
                                                  content: item.date
                                                }
                                              ]
                                            },
                                            {
                                              elem: 'second-line',
                                              content: [
                                                {
                                                  elem: 'title',
                                                  content: item.title
                                                },
                                                {
                                                  elem: 'actions',
                                                  content: {
                                                    block: 'actions',
                                                    content: [
                                                      {
                                                        block: 'action-item',
                                                        mods: { type: 'star' },
                                                        content: {
                                                          block: 'nt-icon',
                                                          mods: { name: 'star' }
                                                        }
                                                      },
                                                      {
                                                        block: 'action-item',
                                                        mods: { type: 'arrow' },
                                                        content: {
                                                          block: 'nt-icon',
                                                          mods: { name: 'share' }
                                                        }
                                                      }
                                                    ]
                                                  }
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          elem: 'introtext',
                                          content: item.introtext
                                        },
                                        {
                                          elem: 'preview',
                                          content: {
                                            block: 'image',
                                            url: '../../demo/images/news-item__preview_01.jpg'
                                          }
                                        },
                                        {
                                          elem: 'footer',
                                          content: [
                                            {
                                              block: 'like',
                                              mix: { block: 'news-item', elem: 'like' },
                                              content: {
                                                block: 'nt-icon',
                                                mods: { name: 'like' }
                                              }
                                            },
                                            {
                                              block: 'stats',
                                              mix: { block: 'news-item', elem: 'stats' },
                                              content: [
                                                {
                                                  block: 'stats-item',
                                                  content: [
                                                    {
                                                      block: 'nt-icon',
                                                      mix: { block: 'stats-item', elem: 'icon' },
                                                      mods: { name: 'rating' }
                                                    },
                                                    {
                                                      elem: 'title',
                                                      content: item.unknown
                                                    }
                                                  ]
                                                },
                                                {
                                                  block: 'stats-item',
                                                  content: [
                                                    {
                                                      block: 'nt-icon',
                                                      mix: { block: 'stats-item', elem: 'icon' },
                                                      mods: { name: 'on-topic' }
                                                    },
                                                    {
                                                      elem: 'title',
                                                      content: item.comments
                                                    }
                                                  ]
                                                },
                                                {
                                                  block: 'stats-item',
                                                  content: [
                                                    {
                                                      block: 'nt-icon',
                                                      mix: { block: 'stats-item', elem: 'icon' },
                                                      mods: { name: 'comments' }
                                                    },
                                                    {
                                                      elem: 'title',
                                                      content: item.questions
                                                    }
                                                  ]
                                                }
                                              ]
                                            },
                                            {
                                              block: 'related',
                                              mix: { block: 'news-item', elem: 'related' },
                                              content: [
                                                {
                                                  block: 'link',
                                                  mix: { block: 'related', elem: 'link' },
                                                  url: '#',
                                                  content: [
                                                    {
                                                      block: 'related',
                                                      elem: 'count',
                                                      content: item.imgCount
                                                    },
                                                    {
                                                      block: 'related',
                                                      elem: 'title',
                                                      content: ' картинок'
                                                    }
                                                  ]
                                                },
                                                {
                                                  block: 'link',
                                                  mix: { block: 'related', elem: 'link' },
                                                  url: '#',
                                                  content: [
                                                    {
                                                      block: 'related',
                                                      elem: 'count',
                                                      content: item.onTopic
                                                    },
                                                    {
                                                      block: 'related',
                                                      elem: 'title',
                                                      content: ' по теме'
                                                    }
                                                  ]
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  }
                                    : {
                                    elem: 'col',
                                    mods: { md: 6 },
                                    content: {
                                      block: 'news-item',
                                      mods: { type: 'narrow' },
                                      content: [
                                        {
                                          elem: 'preview',
                                          content: {
                                            block: 'image',
                                            url: '../../demo/images/news-item__preview_01.jpg'
                                          }
                                        },
                                        {
                                          elem: 'header',
                                          mods: { narrow: true },
                                          content: [
                                            {
                                              elem: 'source-icon',
                                              content: [
                                                {
                                                  block: 'source',
                                                  mix: {
                                                    elem: 'icon',
                                                    mods: { type: item.sourceType, 'news-item': true }
                                                  },
                                                  content: {
                                                    block: 'nt-icon',
                                                    mods: { name: item.sourceType }
                                                  }
                                                },
                                                {
                                                  block: 'date',
                                                  mix: { block: 'news-item', elem: 'date', mods: { narrow: true } },
                                                  content: item.date
                                                }
                                              ]
                                            },
                                            {
                                              elem: 'first-line',
                                              content: [
                                                {
                                                  elem: 'source-title',
                                                  content: item.sourceTitle
                                                },
                                                {
                                                  block: 'time2read',
                                                  mix: { block: 'news-item', elem: 'time2read' },
                                                  content: [
                                                    {
                                                      block: 'nt-icon',
                                                      mix: { block: 'time2read', elem: 'icon' },
                                                      mods: { name: 'eye' }
                                                    },
                                                    {
                                                      elem: 'title',
                                                      content: item.timeForReading
                                                    }
                                                  ]
                                                },
                                                {
                                                  elem: 'actions',
                                                  mods: { narrow: true },
                                                  content: {
                                                    block: 'actions',
                                                    content: [
                                                      {
                                                        block: 'action-item',
                                                        mods: { type: 'star' },
                                                        content: {
                                                          block: 'nt-icon',
                                                          mods: { name: 'star' }
                                                        }
                                                      }
                                                    ]
                                                  }
                                                }
                                              ]
                                            },
                                            {
                                              elem: 'second-line',
                                              content: [
                                                {
                                                  elem: 'title',
                                                  content: item.title
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          elem: 'introtext',
                                          mods: { narrow: true },
                                          content: item.introtext
                                        },
                                        {
                                          elem: 'bottom-line',
                                          content: [
                                            {
                                              block: 'like',
                                              mix: { block: 'news-item', elem: 'like' },
                                              content: {
                                                block: 'nt-icon',
                                                mods: { name: 'like' }
                                              }
                                            },
                                            {
                                              block: 'stats',
                                              mix: { block: 'news-item', elem: 'stats', mods: { narrow: true } },
                                              content: [
                                                {
                                                  block: 'stats-item',
                                                  content: [
                                                    {
                                                      block: 'nt-icon',
                                                      mix: { block: 'stats-item', elem: 'icon' },
                                                      mods: { name: 'rating' }
                                                    },
                                                    {
                                                      elem: 'title',
                                                      content: item.unknown
                                                    }
                                                  ]
                                                },
                                                {
                                                  block: 'stats-item',
                                                  content: [
                                                    {
                                                      block: 'nt-icon',
                                                      mix: { block: 'stats-item', elem: 'icon' },
                                                      mods: { name: 'on-topic' }
                                                    },
                                                    {
                                                      elem: 'title',
                                                      content: item.comments
                                                    }
                                                  ]
                                                },
                                                {
                                                  block: 'stats-item',
                                                  content: [
                                                    {
                                                      block: 'nt-icon',
                                                      mix: { block: 'stats-item', elem: 'icon' },
                                                      mods: { name: 'comments' }
                                                    },
                                                    {
                                                      elem: 'title',
                                                      content: item.questions
                                                    }
                                                  ]
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          elem: 'footer',
                                          mods: { narrow: true },
                                          content: [
                                            {
                                              block: 'action-item',
                                              mix: { block: 'news-item', elem: 'action-item', mods: { narrow: true } },
                                              mods: { type: 'arrow' },
                                              content: {
                                                block: 'nt-icon',
                                                mods: { name: 'share' }
                                              }
                                            },
                                            {
                                              block: 'related',
                                              mix: { block: 'news-item', elem: 'related', mods: { narrow: true } },
                                              content: [
                                                {
                                                  block: 'link',
                                                  mix: { block: 'related', elem: 'link' },
                                                  url: '#',
                                                  content: [
                                                    {
                                                      block: 'related',
                                                      elem: 'count',
                                                      content: item.imgCount
                                                    },
                                                    {
                                                      block: 'related',
                                                      elem: 'title',
                                                      content: ' картинок'
                                                    }
                                                  ]
                                                },
                                                {
                                                  block: 'link',
                                                  mix: { block: 'related', elem: 'link' },
                                                  url: '#',
                                                  content: [
                                                    {
                                                      block: 'related',
                                                      elem: 'count',
                                                      content: item.onTopic
                                                    },
                                                    {
                                                      block: 'related',
                                                      elem: 'title',
                                                      content: ' по теме'
                                                    }
                                                  ]
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  }
                                })
                            }
                          }
                        ]
                      }
                    ]
                  },
                  {
                    elem: 'col',
                    mix: { block: 'columns', elem: 'aside-right' },
                    mods: { md: 3 },
                    content: {
                      block: 'aside',
                      content: [
                        {
                          block: 'title',
                          mix: { block: 'aside', elem: 'title' },
                          mods: { header: 1, align: 'center' },
                          content: 'Популярные новости'
                        },
                        {
                          block: 'p-news',
                          mix: { block: 'aside', elem: 'p-news' },
                          content: [
                            {
                              title: 'Из Саратова в Москву за колбасой',
                              date: '12 июля 2015',
                              introtext: '<p>Объектом исследования историка Дональда Рейли стали дети, родившиеся в',
                              unknown: '12.1',
                              onTopic: '+15'
                            },
                            {
                              title: 'Из Саратова в Москву за колбасой',
                              date: '12 июля 2015',
                              introtext: '<p>Объектом исследования историка Дональда Рейли стали дети, родившиеся в',
                              unknown: '12.1',
                              onTopic: '+5'
                            },
                            {
                              title: 'Из Саратова в Москву за колбасой',
                              date: '12 июля 2015',
                              introtext: '<p>Объектом исследования историка Дональда Рейли стали дети, родившиеся в',
                              unknown: '12.1',
                              onTopic: '+5'
                            },
                            {
                              title: 'Из Саратова в Москву за колбасой',
                              date: '12 июля 2015',
                              introtext: '<p>Объектом исследования историка Дональда Рейли стали дети, родившиеся в',
                              unknown: '12.1',
                              onTopic: '+5'
                            },
                            {
                              title: 'Из Саратова в Москву за колбасой',
                              date: '12 июля 2015',
                              introtext: '<p>Объектом исследования историка Дональда Рейли стали дети, родившиеся в',
                              unknown: '12.1',
                              onTopic: '+5'
                            }
                          ].map(function(item) {
                              return {
                                block: 'p-news-item',
                                content: [
                                  {
                                    elem: 'title',
                                    content: {
                                      block: 'title',
                                      mods: { header: 1 },
                                      content: item.title
                                    }
                                  },
                                  {
                                    elem: 'date',
                                    content: {
                                      block: 'date',
                                      content: item.date
                                    }
                                  },
                                  {
                                    elem: 'introtext',
                                    content: item.introtext
                                  },
                                  {
                                    elem: 'footer',
                                    content: [
                                      {
                                        block: 'link',
                                        mix: [{ block: 'related', elem: 'link' }, { block: 'p-news-item', elem: 'related' } ],
                                        url: '#',
                                        content: [
                                          {
                                            block: 'related',
                                            elem: 'count',
                                            content: item.onTopic
                                          },
                                          {
                                            block: 'related',
                                            elem: 'title',
                                            content: ' по теме'
                                          }
                                        ]
                                      },
                                      {
                                        block: 'stats',
                                        mix: { block: 'p-news-item', elem: 'stats' },
                                        content: [
                                          {
                                            block: 'stats-item',
                                            content: [
                                              {
                                                block: 'nt-icon',
                                                mix: { block: 'stats-item', elem: 'icon' },
                                                mods: { name: 'rating' }
                                              },
                                              {
                                                elem: 'title',
                                                content: item.unknown
                                              }
                                            ]
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              };
                            })
                        }
                      ]
                    }
                  }
                ]
              }
            }
          }/*,
           {
           block: 'container',
           content: [
           {
           block : 'row',
           attrs: { title : 'grid test' },
           content : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(function(item) {
           return {
           elem: 'col',
           mods: { md: 1, ms: 4, xs: 12 },
           content: {
           tag: 'div',
           attrs: { style: 'background-color: rgba(255, 128, 0, .3); height 30px; margin: 15px 0' },
           content: item
           }
           }
           })
           }
           ]
           }*/
        ]
      },
      {
        elem: 'overlay'
      }
    ]
  }
};